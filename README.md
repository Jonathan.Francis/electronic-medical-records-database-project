# Electronic Medical Records Database

1. Link to Front End Web Application - https://gitlab.com/Jonathan.Francis/electronic-medical-records-project-frontend
2. Link to Back End Web Application - https://gitlab.com/Jonathan.Francis/electronic-medical-records-back-end-project
3. Link To Database - https://gitlab.com/Jonathan.Francis/electronic-medical-records-database-project

# Follow Details below to get MySQL connected to your Back End/Front End Project. See Step 1 & 2 below.

Run the below commands on your workbench or MySQL command line to create a new user for node app

NOTE:
Once Step 1 & Step 2 below are completed and connected with your Back End. You will be able to log into the Front End React Project - using the credientials -
To login email is "johndoe@gmail.com", password is "password".
Reminder For the login credientials to work , please follow the instructions from the Back End & Database Link above, will need the Back End set up, along with the Database details imported into MySQL

# Step 1

USE mysql;
CREATE USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost';
flush privileges;


# Step 2 - Select either option 1 or 2 (Option 2 is the preferred method)


1. Option 1 - If you choose option 1 here are the steps:

********
a) Enter the following commands:
    CREATE DATABASE emr;
    USE emr;
******** 
b) Please note you may either copy & paste the tables/values from the Back End Project - see link above for Back End Project & copy tables/values from the folder named database/info.sql. In this folder named database you can find the tables/values in the file named info.sql. BUT DO NOT COPY TABLES/VALUES BELOW, the password value in the password column of the USER (table) below will give you an error. These tables/values from info.sql will need to be entered into your MySQL emr DATABASE. 

********
2. Option 2 - The preferred method will be to import directly the emr.sql file from this Database Project into your MySQL emr DATABASE.
Once imported directly into the MySQL workbench , enter the following command in MySQL workbench to access the Database and table/values:
USE emr;



********
# NOTES - READ ONLY

**NOTE DO NOT COPY TABLES/VALUES BELOW into MySQL emr DATABASE - for read only - USE emr.sql file in this project to import into MySQL emr DATABASE**

/***Table 1 -- Care Provider Data**/
CREATE TABLE IF NOT EXISTS user (
    userID INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(255) DEFAULT '',
    firstName VARCHAR(255) NOT NULL DEFAULT '',
    lastName VARCHAR(255) NOT NULL DEFAULT '',
    email VARCHAR(255) NOT NULL DEFAULT '',
    password VARCHAR(255) NOT NULL DEFAULT '',
    phoneNumber VARCHAR(11) NOT NULL DEFAULT '',
    isAdmin BOOLEAN NOT NULL DEFAULT 0
);

/***Table 1 -- Care Provider Data**/
INSERT INTO user VALUES
(1,"Dr","John","Doe","johndoe@gmail.com","$2b$10$XzAKPIdOPilrrsuDd3pdLO9syohjO1JDmZCJfFzICBnhysv6QPVIi","5195556666",1),
(2,"Dr","Jane","Doe","janedoe@gmail.com","$2b$10$XzAKPIdOPilrrsuDd3pdLO9syohjO1JDmZCJfFzICBnhysv6QPVIi","5195556776",1),
(3,"Mr","Brad","Pitt","test@gmail.com","$2b$10$XzAKPIdOPilrrsuDd3pdLO9syohjO1JDmZCJfFzICBnhysv6QPVIi","5195556666",0);

/**Table 2 - Searching for Appointment Info :: Information to be provided when searching for Appointment Information**/
CREATE TABLE IF NOT EXISTS appointment (
    appointmentNumber INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    patientID INT UNSIGNED  NOT NULL,
    careProviderID INT UNSIGNED NOT NULL,
    date DATETIME NOT NULL DEFAULT '1111-11-11',
    type VARCHAR(255) NOT NULL DEFAULT '',
    location VARCHAR(255) NOT NULL DEFAULT '',
    status VARCHAR(255) NOT NULL DEFAULT ''
    );				


/**Table 2 - Searching for Appointment Info :: Information to be provided when searching for Appointment Information**/
INSERT INTO appointment VALUES
(1000,1,2,"2021-07-29 10:11:13","Routine Check-Up","Family Section - C Wing","Not Completed"),
(1002,2,3,"2021-07-30 11:12:13","Covid-19 Follow-Up","Covid-19 Section - E Wing","Not Completed"),
(1003,3,4,"2021-07-31 12:13:13","Post-Surgery Follow-Up","Surgery Section - A Wing","Not Completed"),
(1004,4,5,"2021-08-01 13:14:13","Pre-Surgery Consultation","Family Section - D Wing","Not Completed");

/***Table 3 - Adding a Patient/Find Patient**/
CREATE TABLE IF NOT EXISTS patient (
    patientID INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    healthCardNumber VARCHAR(10) NOT NULL DEFAULT '',
    firstName VARCHAR(255) NOT NULL DEFAULT '',
    lastName VARCHAR(255) NOT NULL DEFAULT '',
    gender VARCHAR(255) NOT NULL DEFAULT '',
    email VARCHAR(255) NOT NULL DEFAULT '',
    birthDate DATETIME NOT NULL DEFAULT '1111-11-11',
    phoneNumber VARCHAR(11) NOT NULL DEFAULT ''
    );

/***Table 3 - Adding a Patient/Find Patient**/
INSERT INTO patient VALUES
(1,"1111111111","Robin","William","Male","RobinWilliam@gmail.com","1965-12-13 10.10.14","4164233313"),
(2,"1111111112","Rosy","William","Female","RachaelWilliam@gmail.com","1965-12-14 12.10.12","4164233314"),
(3,"1111111113","Jordan","Michael","Male","Mj@gmail.com","1963-11-13 13.10.12","4164333113"),
(4,"1111111114","Jordana","William","Female","JW@gmail.com","1964-10-13 14.10.12","4165535514");

/***Table 4 - To View Patient Medical History**/
CREATE TABLE IF NOT EXISTS medical (
    medicalRecord INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    patientID INT UNSIGNED  NOT NULL,
    allergies VARCHAR(255) NOT NULL DEFAULT '',
    prescriptions VARCHAR(255) NOT NULL DEFAULT '',
    immunizationStatus VARCHAR(255) NOT NULL DEFAULT '',
    diagnosis VARCHAR(255) NOT NULL DEFAULT ''
    );

/***Table 4 - To View Patient Medical History**/
INSERT INTO medical VALUES
(204345,1,"Mild : Peanut Butter/Nuts","Cholesterol Pills","Received First Dose","Asymptomatic to Covid-19"),  
(204346,2,"Severe : Pollen","None","Have not receivd any Doses yet","Negative to Covid-19"), 
(204347,3,"Mild : Dust","Allergy Pills - Reactine","Fully Vacinated","Negative to Covid-19"), 
(204348,4,"Light : Strawberries","Pills for Mild Seizure","Not Vacinated","Positive to Covid-19"); 


/***Table 5 - Patient Billing Information**/
CREATE TABLE IF NOT EXISTS billing (
    invoice INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    patientID INT UNSIGNED  NOT NULL,
    information VARCHAR(255) NOT NULL DEFAULT '',
    amount DECIMAL(7, 2) NOT NULL,
    paymentMethod VARCHAR(255) NOT NULL DEFAULT '',
    status VARCHAR(255) NOT NULL DEFAULT ''
    );

/***Table 5 - Patient Billing Information**/
INSERT INTO billing VALUES
(7000,1,"Routine Check-Up",100.00,"visa","paid"),
(7001,2,"Covid-19 Follow-Up",200.00,"amex","paid"),
(7002,3,"Post-Surgery Follow-Up",300.00,"visa","paid"),
(7003,4,"Pre-Surgery Consultation",200.00,"amex","paid");

/***Table 6 - Patient Notes**/
CREATE TABLE IF NOT EXISTS notes (
    notesID INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    patientID INT UNSIGNED  NOT NULL,
    date DATETIME NOT NULL,
    information MEDIUMTEXT NOT NULL
    );

/***Table 6 - Patient Notes**/
INSERT INTO notes VALUES
(1,1,CURRENT_TIMESTAMP,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
(2,1,CURRENT_TIMESTAMP,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
(3,2,CURRENT_TIMESTAMP,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
(4,3,CURRENT_TIMESTAMP,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
(5,4,CURRENT_TIMESTAMP,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');

/*** Table 7 - Radiology Imaging Information ***/
CREATE TABLE IF NOT EXISTS radiology (
    id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    patientID INT UNSIGNED  NOT NULL,
    documentTitle VARCHAR(255) NOT NULL DEFAULT '',
    document varchar(255) NOT NULL,
    notes VARCHAR(255) NOT NULL DEFAULT '',
    takenDate DATE NOT NULL DEFAULT '1111-11-11'
    );

/*** Table 7 - Radiology Imaging Information ***/
INSERT INTO radiology VALUES
(101,1,"Ankle X Ray","/images/radiology/ankleXray.jpeg","bike accident, no fracture as occured","2019-04-20"),
(102,1,"Hip X Ray","/images/radiology/hipXray.jpeg","Pre-Hip replacement","2018-07-10"),
(103,2,"Fractured Ribs","/images/radiology/fracturedRibsXray.jpeg","Boxing Accident","2020-02-07"),
(104,3,"Misaligned Spine Before Treatment","/images/radiology/MisalignedSpine.png","Before treatment starts - looking at issues that will be attended to","2019-09-10"),
(105,3,"Realigned Spine After Treatment","/images/radiology/FixedSpine.png","Treatment Complete","2020-06-10");

/*** Table 8 - Test Result Information ***/
CREATE TABLE IF NOT EXISTS testResults (
    id INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    patientID INT UNSIGNED  NOT NULL,
    documentTitle VARCHAR(255) NOT NULL DEFAULT '',
    document VARCHAR(255) NOT NULL,
    notes VARCHAR(255) NOT NULL DEFAULT '',
    takenDate DATE NOT NULL DEFAULT '1111-11-11'
    );

/*** Table 8 - Test Result Information ***/
INSERT INTO testResults VALUES
(201,1,"Blood Test","/images/testResults/bloodTest.png","Need to check for any vitamin deficency","2019-04-20"),
(202,1,"Allergy Test","/images/testResults/allergyTest.png","Allergies are listed in the document","2018-07-10"),
(203,2,"Covid Test","/images/testResults/covidTest.jpeg","Needed for work","2020-02-07"),
(204,3,"Urine Sample","/images/testResults/UrineSample.png","Work Related requirement","2019-09-12");

/*** Table 9 - History ***/
CREATE TABLE IF NOT EXISTS history (
    historyID INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    patientID INT UNSIGNED  NOT NULL,
    date DATETIME NOT NULL,
    information TEXT NOT NULL
);
