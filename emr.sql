CREATE DATABASE  IF NOT EXISTS `emr` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `emr`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: localhost    Database: emr
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `appointment`
--

DROP TABLE IF EXISTS `appointment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `appointment` (
  `appointmentNumber` int unsigned NOT NULL AUTO_INCREMENT,
  `patientID` int unsigned NOT NULL,
  `careProviderID` int unsigned NOT NULL,
  `date` datetime NOT NULL DEFAULT '1111-11-11 00:00:00',
  `type` varchar(255) NOT NULL DEFAULT '',
  `location` varchar(255) NOT NULL DEFAULT '',
  `status` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`appointmentNumber`)
) ENGINE=InnoDB AUTO_INCREMENT=1005 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointment`
--

LOCK TABLES `appointment` WRITE;
/*!40000 ALTER TABLE `appointment` DISABLE KEYS */;
INSERT INTO `appointment` VALUES (1000,1,2,'2021-07-29 10:11:13','Routine Check-Up','Family Section - C Wing','Not Completed'),(1002,2,3,'2021-07-30 11:12:13','Covid-19 Follow-Up','Covid-19 Section - E Wing','Not Completed'),(1003,3,4,'2021-07-31 12:13:13','Post-Surgery Follow-Up','Surgery Section - A Wing','Not Completed'),(1004,4,5,'2021-08-01 13:14:13','Pre-Surgery Consultation','Family Section - D Wing','Not Completed');
/*!40000 ALTER TABLE `appointment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `billing`
--

DROP TABLE IF EXISTS `billing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `billing` (
  `invoice` int unsigned NOT NULL AUTO_INCREMENT,
  `patientID` int unsigned NOT NULL,
  `information` varchar(255) NOT NULL DEFAULT '',
  `amount` decimal(7,2) NOT NULL,
  `paymentMethod` varchar(255) NOT NULL DEFAULT '',
  `status` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`invoice`)
) ENGINE=InnoDB AUTO_INCREMENT=7004 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `billing`
--

LOCK TABLES `billing` WRITE;
/*!40000 ALTER TABLE `billing` DISABLE KEYS */;
INSERT INTO `billing` VALUES (7000,1,'Routine Check-Up',100.00,'visa','paid'),(7001,2,'Covid-19 Follow-Up',200.00,'amex','paid'),(7002,3,'Post-Surgery Follow-Up',300.00,'visa','paid'),(7003,4,'Pre-Surgery Consultation',200.00,'amex','paid');
/*!40000 ALTER TABLE `billing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `history`
--

DROP TABLE IF EXISTS `history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `history` (
  `historyID` int unsigned NOT NULL AUTO_INCREMENT,
  `patientID` int unsigned NOT NULL,
  `date` datetime NOT NULL,
  `information` text NOT NULL,
  PRIMARY KEY (`historyID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history`
--

LOCK TABLES `history` WRITE;
/*!40000 ALTER TABLE `history` DISABLE KEYS */;
/*!40000 ALTER TABLE `history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medical`
--

DROP TABLE IF EXISTS `medical`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medical` (
  `medicalRecord` int unsigned NOT NULL AUTO_INCREMENT,
  `patientID` int unsigned NOT NULL,
  `allergies` varchar(255) NOT NULL DEFAULT '',
  `prescriptions` varchar(255) NOT NULL DEFAULT '',
  `immunizationStatus` varchar(255) NOT NULL DEFAULT '',
  `diagnosis` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`medicalRecord`)
) ENGINE=InnoDB AUTO_INCREMENT=204349 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medical`
--

LOCK TABLES `medical` WRITE;
/*!40000 ALTER TABLE `medical` DISABLE KEYS */;
INSERT INTO `medical` VALUES (204345,1,'Mild : Peanut Butter/Nuts','Cholesterol Pills','Received First Dose','Asymptomatic to Covid-19'),(204346,2,'Severe : Pollen','None','Have not receivd any Doses yet','Negative to Covid-19'),(204347,3,'Mild : Dust','Allergy Pills - Reactine','Fully Vacinated','Negative to Covid-19'),(204348,4,'Light : Strawberries','Pills for Mild Seizure','Not Vacinated','Positive to Covid-19');
/*!40000 ALTER TABLE `medical` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notes`
--

DROP TABLE IF EXISTS `notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notes` (
  `notesID` int unsigned NOT NULL AUTO_INCREMENT,
  `patientID` int unsigned NOT NULL,
  `date` datetime NOT NULL,
  `information` mediumtext NOT NULL,
  PRIMARY KEY (`notesID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notes`
--

LOCK TABLES `notes` WRITE;
/*!40000 ALTER TABLE `notes` DISABLE KEYS */;
INSERT INTO `notes` VALUES (1,1,'2021-08-13 13:09:58','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),(2,1,'2021-08-13 13:09:58','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),(3,2,'2021-08-13 13:09:58','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),(4,3,'2021-08-13 13:09:58','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),(5,4,'2021-08-13 13:09:58','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
/*!40000 ALTER TABLE `notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `patient` (
  `patientID` int unsigned NOT NULL AUTO_INCREMENT,
  `healthCardNumber` varchar(10) NOT NULL DEFAULT '',
  `firstName` varchar(255) NOT NULL DEFAULT '',
  `lastName` varchar(255) NOT NULL DEFAULT '',
  `gender` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `birthDate` datetime NOT NULL DEFAULT '1111-11-11 00:00:00',
  `phoneNumber` varchar(11) NOT NULL DEFAULT '',
  PRIMARY KEY (`patientID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient`
--

LOCK TABLES `patient` WRITE;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` VALUES (1,'1111111111','Robin','William','Male','RobinWilliam@gmail.com','1965-12-13 10:10:14','4164233313'),(2,'1111111112','Rosy','William','Female','RachaelWilliam@gmail.com','1965-12-14 12:10:12','4164233314'),(3,'1111111113','Jordan','Michael','Male','Mj@gmail.com','1963-11-13 13:10:12','4164333113'),(4,'1111111114','Jordana','William','Female','JW@gmail.com','1964-10-13 14:10:12','4165535514');
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `radiology`
--

DROP TABLE IF EXISTS `radiology`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `radiology` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `patientID` int unsigned NOT NULL,
  `documentTitle` varchar(255) NOT NULL DEFAULT '',
  `document` varchar(255) NOT NULL,
  `notes` varchar(255) NOT NULL DEFAULT '',
  `takenDate` date NOT NULL DEFAULT '1111-11-11',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `radiology`
--

LOCK TABLES `radiology` WRITE;
/*!40000 ALTER TABLE `radiology` DISABLE KEYS */;
INSERT INTO `radiology` VALUES (101,1,'Ankle X Ray','/images/radiology/ankleXray.jpeg','bike accident, no fracture as occured','2019-04-20'),(102,1,'Hip X Ray','/images/radiology/hipXray.jpeg','Pre-Hip replacement','2018-07-10'),(103,2,'Fractured Ribs','/images/radiology/fracturedRibsXray.jpeg','Boxing Accident','2020-02-07'),(104,3,'Misaligned Spine Before Treatment','/images/radiology/MisalignedSpine.png','Before treatment starts - looking at issues that will be attended to','2019-09-10'),(105,3,'Realigned Spine After Treatment','/images/radiology/FixedSpine.png','Treatment Complete','2020-06-10');
/*!40000 ALTER TABLE `radiology` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testresults`
--

DROP TABLE IF EXISTS `testresults`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `testresults` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `patientID` int unsigned NOT NULL,
  `documentTitle` varchar(255) NOT NULL DEFAULT '',
  `document` varchar(255) NOT NULL,
  `notes` varchar(255) NOT NULL DEFAULT '',
  `takenDate` date NOT NULL DEFAULT '1111-11-11',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testresults`
--

LOCK TABLES `testresults` WRITE;
/*!40000 ALTER TABLE `testresults` DISABLE KEYS */;
INSERT INTO `testresults` VALUES (201,1,'Blood Test','/images/testResults/bloodTest.png','Need to check for any vitamin deficency','2019-04-20'),(202,1,'Allergy Test','/images/testResults/allergyTest.png','Allergies are listed in the document','2018-07-10'),(203,2,'Covid Test','/images/testResults/covidTest.jpeg','Needed for work','2020-02-07'),(204,3,'Urine Sample','/images/testResults/UrineSample.png','Work Related requirement','2019-09-12');
/*!40000 ALTER TABLE `testresults` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `userID` int unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT '',
  `firstName` varchar(255) NOT NULL DEFAULT '',
  `lastName` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `phoneNumber` varchar(11) NOT NULL DEFAULT '',
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Dr','John','Doe','johndoe@gmail.com','$2b$10$XzAKPIdOPilrrsuDd3pdLO9syohjO1JDmZCJfFzICBnhysv6QPVIi','5195556666',1),(2,'Dr','Jane','Doe','janedoe@gmail.com','$2b$10$XzAKPIdOPilrrsuDd3pdLO9syohjO1JDmZCJfFzICBnhysv6QPVIi','5195556776',1),(3,'Mr','Brad','Pitt','test@gmail.com','$2b$10$XzAKPIdOPilrrsuDd3pdLO9syohjO1JDmZCJfFzICBnhysv6QPVIi','5195556666',0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-13 13:26:58
